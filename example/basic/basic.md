# Swabe Basic Sample

Defines a basic server with a single end point using connect

## Requirements

* [connect](https://www.npmjs.com/package/connect)

[back](../examples.md)