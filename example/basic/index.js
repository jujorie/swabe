'use strict';

const connect = require('connect');
const app = connect();
const http = require('http');
const {resolve} = require('path');
const {SwaggerServer} = require('../../lib');

SwaggerServer.create()
    .description('Basic Sample server')
    .version('0.1.0')
    .title('Basic API')
    .controller(resolve(__dirname, './basic.controller.js'))

    .initialize(app)
    .then(() => {
        http.createServer(app).listen(8080, () => {
            console.log('Server is up on 8080');
        });
    })
    .catch((err) => {
        console.log(err.toString());
    });
