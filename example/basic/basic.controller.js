'use strict';

const {SwaggerAction} = require('../../lib');

const data = [
    {id: 1, name: 'one'},
    {id: 2, name: 'two'}
];

module.exports = SwaggerAction
    .create()

    .define('getInfo')
    .requestHandler(SwaggerAction.jsonResponse(() => {
        return data;
    }))

    .define('getInfo')
    .pathParameter('id', 'id of info')
    .requestHandler(SwaggerAction.jsonResponse(({id}) => {
        return data.find((item) => item.id == id);
    }))

    .define('getStatus')
    .url('/status')
    .requestHandler(SwaggerAction.jsonResponse(() => {
        return {
            source: 'status',
            data
        };
    }))

    .getSwaggerAction();
