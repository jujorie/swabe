'use strict';

const {resolve} = require('path');
const {SwaggerServer, SwaggerAmazonIntegration} = require('../../lib');

module.exports = SwaggerServer.create({
    host: 'www.sample.com'
})
    .description('AWS API Gateway server test')
    .version('0.1.0')
    .title('AWS API Gateway Test')
    .controller(
        resolve(__dirname, './apigateway.controller.js'),
        [SwaggerAmazonIntegration] // Aws api gateway
    );
