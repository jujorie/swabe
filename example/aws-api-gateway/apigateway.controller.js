'use strict';

const {SwaggerAction} = require('../../lib');

module.exports = SwaggerAction
    .create()

    .define('getInfo')
    .requestHandler(SwaggerAction.jsonResponse(() => {
        return 'hello';
    }))

    .getSwaggerAction();
