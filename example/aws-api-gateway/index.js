'use strict';

const connect = require('connect');
const app = connect();
const http = require('http');

const server = require('./apigateway.server');

server
    .initialize(app)
    .then(() => {
        http.createServer(app).listen(8080, () => {
            console.log('Server is up on 8080');
        });
    })
    .catch((err) => {
        console.log(err.toString());
    });
