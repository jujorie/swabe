# Swagger Backend - Swabe

Easy create a backend using swagger

## Table of contents

* [Requirements](#requirements)
* [Dependencies](#dependencies)
* [Installation](#installation)
* [Basic Usage](#basic-usage)
* [Examples](#examples)
* [Commands](#commands)

## Requirements

* [node](https://nodejs.org) - Version v8.0.0+
* [jsdoc](http://usejsdoc.org/index.html)

## Dependencies

* [swagger-tools](https://www.npmjs.com/package/swagger-tools) - For creating routes
* [connect](https://www.npmjs.com/package/connect) - For server middlewares

## Installation

```sh
$ npm install -S @jujorie/swabe
```

## Basic Usage

__sample/server.js__
```javascript
'use strict';

const connect = require('connect');
const app = connect();
const http = require('http');
const {resolve} = require('path');
const {SwaggerServer} = require('@jujorie/swabe');

SwaggerServer.create()
    .description('Basic Sample server')
    .version('0.1.0')
    .title('Basic API')
    .controller(resolve(__dirname, './controller.js'))

    .initialize(app)
    .then(() => {
        http.createServer(app).listen(8080, () => {
            console.log('Server is up on 8080');
        });
    })
    .catch((err) => {
        console.log(err.toString());
    });
```

__sample/controller.js__
```javascript
'use strict';

const {SwaggerAction} = require('@jujorie/swabe');

module.exports = SwaggerAction
    .create()

    .define('getInfo')
    .requestHandler(SwaggerAction.jsonResponse(() => {
        return 'hello';
    }))

    .getSwaggerAction();
```

this will produce a end point at __http://localhost:8080/sample/info__

## Examples

you may check others examples at [here](example/examples.md)

## Commands

For development once cloned the repository can use this npm commands

* [test](#test)
    * [test:only](#test-only)
* [lint](#lint)
* [doc](#doc)

### Test

Run all the tests with coverage analysis

```sh
$ npm run test
```

### Test Only

Run all the tests without coverage analysis

```sh
$ npm run test:only
```

### Lint

Run the linter for the code

```sh
$ npm run lint
```

### doc

Run the document generator

```sh
$ npm run doc
```
