'use strict';

((module) => {
    /* eslint-disable-next-line max-len */
    const AMAZON_API_GATEWAY = require('./swagger-amazon-apigateway-integration.json');
    const inMap = {
        'path': 'path',
        'query': 'querystring',
        'header': 'header'
    };

    /**
     * replace values in all JSON setting
     * @param {object} settings - settings
     * @param {object} context - context vars to be replaced
     * @return {object} replaced value objects
     */
    function replaceParams(settings, context) {
        let textSettings = JSON.stringify(settings);
        Object
            .keys(context)
            .forEach((key) => {
                const value = context[key].toLocaleUpperCase();
                textSettings = textSettings
                    .replace(new RegExp(`##${key}##`, 'g'), value);
            });
        return JSON.parse(textSettings);
    }

    /**
     * Amazon action setup
     * @param {string} verb - Http verb
     * @param {string} url - Url
     * @param {SwaggerObject} metadata - Swagger objects
     * @param {SwaggerDocs} swaggerDocs - swagger doc complete
     */
    function swaggerAmazonIntegration(verb, url, metadata, swaggerDocs) {
        let integration = null;
        const azSetting = replaceParams(
            AMAZON_API_GATEWAY[verb] || AMAZON_API_GATEWAY.default,
            {
                verb
            }
        );
        const setting = {};

        Object.assign(setting, azSetting);
        integration = setting['x-amazon-apigateway-integration'];
        if ('mock' !== integration.type) {
            integration.httpMethod = verb.toLocaleUpperCase();
            integration.uri = integration.uri + url;
            if (metadata.parameters && metadata.parameters.length) {
                metadata.parameters.forEach((param) => {
                    let amznParam;
                    if (param['$ref']) {
                        const ref = param['$ref'];
                        amznParam = swaggerDocs;
                        ref.split('/')
                            .splice(1)
                            .forEach((key) => {
                                amznParam = amznParam[key];
                            });
                    } else {
                        amznParam = param;
                    }
                    if (amznParam) {
                        const inValue = inMap[amznParam.in];
                        if (!inValue) {
                            throw new Error(
                                `Invalid in param '${amznParam.in}'`
                            );
                        }
                        const pKey = `integration.request.`
                            + `${inValue}.${amznParam.name}`;
                        if (!integration.requestParameters) {
                            integration.requestParameters = {};
                        };
                        integration.requestParameters[pKey] =
                            `method.request.${inValue}.${amznParam.name}`;
                    }
                });
            }
        }
        Object.assign(metadata, setting);
    };

    module.exports = swaggerAmazonIntegration;
})(module);
