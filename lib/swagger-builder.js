'use strict';

((module) => {
    const debug = require('debug')('swagger:builder');
    const {basename, dirname, join} = require('path');
    const glob = require('glob');

    /**
     * Swagger configuration builder
     */
    class SwaggerBuilder {
        /**
         * Create new instance of SwaggerBuilder
         * @param {SwaggerObject} swaggerObject
         * - Base swagger object
         * @param {Object} options - Options
         * @param {builder[]} options.builders
         * - Aditional builders
         * @param {string} options.controllersRoot
         * - Where are located the root controllers
         * @param {string} options.controllersExtension
         * - extension of the controllers files
         * @param {Object} options.swaggerRouterOptions
         * - Swagger options
         */
        constructor(
            swaggerObject,
            {
                builders = [],
                swaggerRouterOptions = null,
                controllersRoot = null,
                controllersExtension = '.controller.js'
            } = {}
        ) {
            this._swaggerObject = swaggerObject;
            this._builders = builders;
            this._swaggerRouterOptions = swaggerRouterOptions;
            this._controllersRoot = controllersRoot;
            this._controllersExtension = controllersExtension;
        }

        /**
         * Get url from name
         * @param {string} file
         * @return {string}
         */
        _buildUrlFromControllerFilename(file) {
            let prefix = '/';
            let url = '';
            if (this._controllersRoot
                && dirname(file) !== this._controllersRoot) {
                prefix = file.replace(this._controllersRoot, '');
                prefix = dirname(prefix);
            }
            url = basename(file, this._controllersExtension)
                .split(/(?=[A-Z])/)
                .map((p) => p.toLocaleLowerCase())
                .join('/');

            url = join(prefix, url).replace(/\\/g, '/');
            if (0 === url.indexOf('/')) {
                url = url.substr(1);
            }

            return url;
        }

        /**
         * Build url of the action
         * @param {string} root - Root directory of the controller
         * @param {string} actionName - action name
         * @param {Object} metadata - metadata
         * @return {string}
         */
        _buildActionUrl(root, actionName, metadata) {
            if (metadata['x-url']) {
                let base = dirname(root);
                if (base === '.') {
                    base = null;
                }
                const url = metadata['x-url'];
                delete metadata['x-url'];
                const actionUrl = [base, url].join('');

                return '/' === actionUrl[0]
                    ? actionUrl
                    : '/' + actionUrl;
            }

            const rootParts = root.split('/');
            const parts = [root];
            //
            //  Shortcut for name of the action
            //
            if (rootParts[rootParts.length - 1] !== actionName) {
                parts.push(actionName);
            }

            if (metadata.parameters && metadata.parameters.length) {
                metadata.parameters
                    .forEach((parameter) => {
                        if ('path' === parameter.in) {
                            parts.push(`{${parameter.name}}`);
                        }
                    });
            }
            return '/' + parts.join('/');
        }

        /**
         * Get the verb and action from method name
         * @param {string} methodName
         * @return {{verb:string, action:string}|null} verb and action
         * @private
         */
        _getVerbAndActionFromMethodName(methodName) {
            let verb = null;
            let action = null;
            /* eslint-disable-next-line max-len */
            const verbRx = /^(get|update|post|patch|put|delete|options)([A-Z].*?)$/g;
            const matches = verbRx.exec(methodName);
            if (!matches) {
                debug(`Can't build ${methodName}`);
                return null;
            }
            verb = matches[1].toLocaleLowerCase();
            action = matches[2].toLocaleLowerCase().replace(/\$/gi, '');
            return {
                verb,
                action
            };
        }

        /**
         * Update the swagger document from the action
         * @param {string} controllerRootUrl - controller url
         * @param {string} controllerFilename - filename of controller
         * @param {string} methodName - method name
         * @param {Object} actions - available actions must include methodName
         * @private
         */
        _createAction(
            controllerRootUrl,
            controllerFilename,
            methodName,
            actions
        ) {
            const result = this._getVerbAndActionFromMethodName(methodName);
            if (result) {
                let verbAction = 'get';
                const {verb, action} = result;
                const metadata = actions._metadata[methodName];
                const actionUrl = this._buildActionUrl(
                    controllerRootUrl,
                    action,
                    metadata
                );

                if (!this._swaggerObject.paths) {
                    this._swaggerObject.paths = {};
                }

                if (!this._swaggerObject.paths[actionUrl]) {
                    this._swaggerObject.paths[actionUrl] = {
                        'x-swagger-router-controller': controllerFilename
                    };
                }
                verbAction = this._swaggerObject.paths[actionUrl];
                verbAction[verb] = {
                    operationId: methodName
                };

                this._builders.forEach((builder) => {
                    builder(verb, actionUrl, metadata, this._swaggerObject);
                });

                Object.assign(verbAction[verb], metadata);
                debug(`  * Found '${verb}' ${action} @ ${actionUrl}`);
            }
        }

        /**
         * Build a Swagger document objec from init doc
         * @param {object} init
         * @param {string[]|string} init.path
         */
        build(init) {
            let paths;
            const me = this;
            const controllerPaths = {};

            if (Array.isArray(init.path)) {
                paths = init.path;
            } else {
                paths = [init.path];
            }
            paths.forEach((pattern) => {
                if (this._swaggerRouterOptions) {
                    const opt = this._swaggerRouterOptions;
                    if (!opt.controllers) {
                        opt.controllers = [];
                    }
                    if (!Array.isArray(opt.controllers)) {
                        opt.controllers = [opt.controllers];
                    }
                };

                const files = glob.sync(pattern, {});
                files.forEach((file) => {
                    const dir = dirname(file);
                    const actions = require(file);
                    const controllerFilename = basename(file, '.js');
                    const controllerRootUrl = this
                        ._buildUrlFromControllerFilename(file);

                    debug(
                        `* Build from '${controllerFilename}' `
                        + `on root ${controllerRootUrl}`
                    );

                    controllerPaths[dir] = true;
                    Object.keys(actions)
                        .filter((key) => '_metadata' !== key)
                        .forEach((key) => {
                            this._createAction(
                                controllerRootUrl,
                                controllerFilename,
                                key,
                                actions
                            );
                        });
                });
            });

            if (me._swaggerRouterOptions) {
                Object
                    .keys(controllerPaths)
                    .forEach((key) => {
                        me._swaggerRouterOptions.controllers.push(key);
                    });
            }
        }

        /**
         * Create new SwaggerBuilder instance
         * @param {SwaggerObject} swaggerObject
         * @param {Object} options - Options
         * @param {builder[]} options.builders - Aditional builders
         * @param {Object} options.swaggerRouterOptions - Swagger options
         * @param {string} options.controllerRoot
         * - Where are located the root controllers
         * @return {SwaggerBuilder} - New instance
         * @static
         */
        static create(
            swaggerObject,
            {
                builders = [],
                swaggerRouterOptions = null,
                controllersRoot = null,
            } = {}
        ) {
            return new SwaggerBuilder(swaggerObject, {
                builders,
                swaggerRouterOptions,
                controllersRoot
            });
        }
    }

    /**
     * Generic builder of param
     * @callback param_builder_callbacks
     * @param {string} verb
     * @param {string} url
     * @param {SwaggerObject} metedata
     */
    module.exports = SwaggerBuilder;
})(module);
