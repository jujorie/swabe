'use strict';

((module) => {
    const DEFAULT_RESPONSES = require('./swagger-default-responses.json');
    const SwaggerHttpError = require('./swagger-http-error');
    const EMPTY_RESPONSE_HANDLER = (req, res) => {
        res.end();
    };

    /**
     * Swagger action
     */
    class SwaggerAction {
        /**
         * Create a new instance of SwaggerAction
         * @param {object} [defaultResponses] - Default response for the action
         */
        constructor(defaultResponses = DEFAULT_RESPONSES) {
            this._action = {};
            this._metadata = {};
            this._lastName = null;
            this.defaultResponses = defaultResponses;
        }

        /**
         * Defines a new swagger action
         * @param {string} name - verb and name to be usexx
         * @param {object} [metadata={}] - Options of the actionn swagger
         * @param {actionRequestCallback} fn - action body
         * @return {SwaggerAction} to link
         */
        define(name, metadata = {}, fn = null) {
            const buildedMetadata = {};

            while (this._action[name]) {
                name += '$';
            }

            Object.assign(buildedMetadata,
                JSON.parse(JSON.stringify(this.defaultResponses))
            );
            Object.assign(buildedMetadata,
                JSON.parse(JSON.stringify(metadata))
            );

            if (!fn) {
                this._action[name] = EMPTY_RESPONSE_HANDLER;
            } else {
                this._action[name] = fn;
            }
            this._metadata[name] = buildedMetadata;
            this._lastName = name;

            return this;
        }

        /**
         * Add a generic parameter
         * @param {string} name - Name of the parameter
         * @param {string} description - Description of the parameter
         * @param {object} options - Options of the parameters
         * @return {SwaggerAction} to link
         */
        parameter(name, description = '', options = {}) {
            const metadata = this._getLastMetadata();
            const parameter = {
                name,
                description
            };
            Object.assign(parameter, options);

            if (!metadata.parameters) {
                metadata.parameters = [];
            }
            metadata.parameters.push(parameter);

            return this;
        }

        /**
         * Build a json body parameter
         * @param {string} name - Name of the parameter
         * @param {string} description - description of the parameter
         * @param {object} schema - ajv object definition
         * @return {SwaggerAction} to link
         */
        bodyJsonParameter(name, description = '', schema = {}) {
            return this.parameter(
                name,
                description, {
                    in: 'body',
                    schema
                });
        }

        /**
         * Create a path parameter
         * @param {string} name - Name of the parameter
         * @param {string} [description=''] - Description of the paranmeter
         * @param {string} [type='string'] - Type of the parameter
         * @return {SwaggerAction} to link
         */
        pathParameter(
            name,
            description = '',
            type = 'string') {
            return this.parameter(name, description, {
                type,
                required: true,
                in: 'path'
            });
        }

        /**
         * Create a path parameter
         * @param {string} name - Name of the parameter
         * @param {string} [description=''] - Description of the paranmeter
         * @param {string} [type='string'] - Type of the parameter
         * @return {SwaggerAction} to link
         */
        queryParameter(
            name,
            description = '',
            type = 'string') {
            return this.parameter(name, description, {
                type,
                in: 'query'
            });
        }

        /**
         * set ref parameter
         * @param {string} path - ref parameter
         * @return {SwaggerAction} to link
         */
        refParameter(path) {
            const metadata = this._getLastMetadata();
            if (!metadata.parameters) {
                metadata.parameters = [];
            }
            metadata.parameters.push({
                '$ref': path
            });
            return this;
        }

        /**
         * Defines a request callback for the action
         * @param {actionRequestCallback} fn - Action request callback
         * @return {SwaggerAction} to link
         */
        requestHandler(fn) {
            this._throwsNoActionDefined();
            this._action[this._lastName] = fn;
            return this;
        }

        /**
         * Define a description for the last defined action
         * @param {string} value - description to be defined
         * @return {SwaggerAction} to link
         */
        description(value) {
            /**
             * @type {{description: string}}
             */
            const metadata = this._getLastMetadata();
            metadata.description = value;
            return this;
        }

        /**
         * Sets the action url
         * @param {string} value - Url value
         * @return {SwaggerAction} to link
         */
        url(value) {
            /**
             * @type {{url: string}}
             */
            const metadata = this._getLastMetadata();
            metadata['x-url'] = value;
            return this;
        }

        /**
         * Adds a summary for the last defined action
         * @param {string} value - summary to be defined
         * @return {SwaggerAction} to link
         */
        summary(value) {
            /**
             * @type {{summary: string}}
             */
            const metadata = this._getLastMetadata();
            metadata.summary = value;
            return this;
        }

        /**
         * Set tags for the last defined action
         * @param  {...string} values - Tags to be added
         * @return {SwaggerAction} to link
         */
        tags(...values) {
            /**
             * @type {{tags: Array<string>}}
             */
            const metadata = this._getLastMetadata();
            if (!metadata.tags) {
                metadata.tags = [];
            }
            metadata.tags.push(...values);
            return this;
        }

        /**
         * Request callback
         * @callback actionRequestCallback
         * @param {Request} req - httpRequest
         * @param {Response} res - httpResponse
         * @param {next} next - end handler
         */


        /**
         * Get swagger action for definition
         * @return {object} - Swagger options
         */
        getSwaggerAction() {
            const action = {
                _metadata: this._metadata
            };
            Object.assign(action, this._action);
            return action;
        }

        /**
         * Define response for a action
         * @param {number} statusCode - Response status code
         * @param {string} description - Name of the response
         * @param {object} schema - response schema
         * @return {SwaggerAction} to link
         */
        response(statusCode, description, schema) {
            const metadata = this._getLastMetadata();

            if (!metadata.responses) {
                metadata.responses = {};
            }
            if (!metadata.responses[`${statusCode}`]) {
                metadata.responses[`${statusCode}`] = {};
            }
            metadata.responses[`${statusCode}`].description = description;
            metadata.responses[`${statusCode}`].schema = JSON.parse(
                JSON.stringify(schema)
            );
            return this;
        }

        /**
         * Get the simplified swagger params
         * @param {Request} req - request object
         * @return {object};
         */
        static getSwaggerParams(req) {
            const params = {};
            if (req.swagger) {
                Object
                    .keys(req.swagger.params)
                    .filter((key) => key !== 'Authorization')
                    .forEach((key) => {
                        const value = req.swagger.params[key].value;
                        if (value) {
                            params[key] = value;
                        }
                    });
            }
            return params;
        }

        /**
         * Create a response handler
         * @param {actionRequestCallback} handler - response function
         * @param {string} contentType - content type of the response
         * @return {actionRequestCallback}
         * @static
         */
        static response(handler, contentType) {
            return async (req, res, next) => {
                res.setHeader('Content-Type', contentType);
                try {
                    const params = SwaggerAction.getSwaggerParams(req);
                    let value = await handler(params, req, res);
                    if (value) {
                        value = JSON.stringify(value);
                    }
                    res.end(value);
                } catch (err) {
                    if (err instanceof SwaggerHttpError) {
                        res.statusCode = err.statusCode;
                    } else {
                        res.statusCode = 400;
                    }
                    res.end(JSON.stringify({
                        error: err.message
                    }));
                }
            };
        }

        /**
         * Response as JSON
         * @param {actionRequestCallback} fn - action body
         * @return {actionRequestCallback}
         * @static
         */
        static jsonResponse(fn) {
            return SwaggerAction.response(fn, 'application/json');
        }

        /**
         * Get default resposes
         * @return {object}
         */
        static get defaultResponses() {
            return JSON.parse(
                JSON.stringify(DEFAULT_RESPONSES)
            );
        }

        /**
         * Get metadata for the last defined action
         * @return {{ tags: Array<string>, description: string }}
         */
        _getLastMetadata() {
            this._throwsNoActionDefined();
            return this._metadata[this._lastName];
        }

        /**
         * Throws an error when there is no action defined
         */
        _throwsNoActionDefined() {
            if (!this._lastName) {
                throw new Error('You must define a action first');
            }
        }

        /**
         * create a new SwaggerAction
         * @param {object} [defaultResponses] - Default response for the action
         * @return {SwaggerAction} - new instance of SwaggerAction
         * @static
         */
        static create(defaultResponses) {
            return new SwaggerAction(defaultResponses);
        }
    }

    module.exports = SwaggerAction;
})(module);
