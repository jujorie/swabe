'use strict';

/**
 * Http error response
 */
class SwaggerHttpError extends Error {
    /**
     * Represents a http error
     * @param {string} message - Error message
     * @param {number} [statusCode=400] - ststus code
     */
    constructor(message, statusCode = 400) {
        super(message);

        /**
         * http status code
         * @type {number}
         */
        this.statusCode = statusCode;
    }
}

module.exports = SwaggerHttpError;
