'use strict';

const {EventEmitter} = require('events');
const {initializeMiddleware} = require('swagger-tools');
const SwaggerBuilder = require('./swagger-builder');

const BASE_DOC = {
    swagger: '2.0',
    info: {
        description: '',
        version: '0.0.0',
        title: ''
    }
};

/**
 * Creates a server
 */
class SwaggerServer extends EventEmitter {
    /**
     * Creates a new instance of SwaggerServer
     * @param {object} [swaggerDoc] - Swagger document
     */
    constructor(swaggerDoc = {}) {
        super();
        this._doc = {};
        Object.assign(
            this._doc,
            JSON.parse(JSON.stringify(BASE_DOC)),
            swaggerDoc
        );
        this._routerOptions = {};
    }

    /**
     * Gets the swagger document
     */
    get document() {
        return this._doc;
    }

    /**
     * Set description
     * @param {string} value - description
     * @return {SwaggerServer}
     */
    description(value) {
        this._initDoc();
        this._doc.info.description = value;
        return this;
    }

    /**
     * Set title
     * @param {string} value - title
     * @return {SwaggerServer}
     */
    title(value) {
        this._initDoc();
        this._doc.info.title = value;
        return this;
    }

    /**
     * Set version
     * @param {string} value - version
     * @return {SwaggerServer}
     */
    version(value) {
        this._initDoc();
        this._doc.info.version = value;
        return this;
    }

    /**
     * Adds a controller
     * @param {string | string[]} pattern - Controller pattern
     * @param {Array} builders - Additional builder
     * @return {SwaggerServer}
     */
    controller(pattern, builders) {
        SwaggerBuilder
            .create(this._doc, {
                swaggerRouterOptions: this._routerOptions,
                builders
            })
            .build({
                path: pattern
            });
        return this;
    }

    /**
     * Initialize swagger doc
     */
    _initDoc() {
        if (!this._doc.info) {
            this._doc.info = {};
        }
    }

    /**
     * Initialize swagger server
     * @param {object} app - Server middleware
     * @param {function}  [onBeforeRouter=null]
     * - function to be called just before the router middleware
     * @return {Promise} resolves when everithigs is ready to work
     */
    initialize(app, onBeforeRouter = null) {
        return new Promise((resolve) => {
            initializeMiddleware(this._doc, (middleware) => {
                app.use(middleware.swaggerMetadata());
                app.use(middleware.swaggerValidator());
                if (typeof onBeforeRouter === 'function') {
                    onBeforeRouter.call(this, app);
                }
                app.use(middleware.swaggerRouter(this._routerOptions));
                app.use(middleware.swaggerUi());
                resolve(middleware);
            });
        });
    }

    /**
     * Creates a new instance of SwaggerServer
     * @param {object} [swaggerDoc] - Swagger document
     * @return {SwaggerServer}
     */
    static create(swaggerDoc) {
        return new SwaggerServer(swaggerDoc);
    }
}

module.exports = SwaggerServer;
