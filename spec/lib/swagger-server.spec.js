'use strict';

describe('swagger-server', () => {
    const {resolve} = require('path');
    const SwaggerServer = require('../../lib/swagger-server');
    const toObj = (o) => JSON.stringify(o, null, 4);


    describe('When has default parameter', () => {
        it('Must use defined document', () => {
            const correct = require('./swagger-server-1.result');
            const doc = SwaggerServer
                .create({
                    swagger: '2.0',
                    info: {
                        description: 'desc',
                        version: '1.0',
                        title: 'title'
                    }
                })
                .document;

            expect(toObj(doc))
                .toEqual(toObj(correct));
        });
    });

    describe('When create a doc from server', () => {
        const testCases = [
            {
                swaggerDoc: {},
                correct: {
                    swagger: '2.0',
                    info: {
                        description: '',
                        version: '0.0.0',
                        title: ''
                    }
                }
            },
            {
                swaggerDoc: {
                    info: null
                },
                correct: {
                    swagger: '2.0',
                    info: null
                }
            },
            ,
            {
                swaggerDoc: {
                    info: null
                },
                correct: {
                    swagger: '2.0',
                    info: {
                        description: 'desc'
                    }
                },
                exec: (s) => s.description('desc')
            }
        ];

        testCases.forEach((testCase, index) => {
            it(`Default doc ${index}`, () => {
                const server = SwaggerServer
                    .create(testCase.swaggerDoc);

                if (testCase.exec) {
                    testCase.exec(server);
                }
                expect(toObj(server.document))
                    .toEqual(toObj(testCase.correct));
            });
        });
    });

    describe('When has to create a server', () => {
        let server = null;
        const app = {
            use() {}
        };

        beforeEach(() => {
            server = SwaggerServer
                .create()
                .description('desc')
                .version('1.0')
                .title('title');
        });

        it('Must add a route', async () => {
            const correct = require('./swagger-server-2.result');
            const doc = server
                .controller(resolve(__dirname, './controllers/*'))
                .document;

            expect(toObj(doc))
                .toEqual(toObj(correct));

            await server
                .initialize(app);
        });

        it('Must to call hook', async () => {
            await server
                .controller(resolve(__dirname, './controllers/*'))
                .initialize(app, (p) => {
                    expect(p).toBe(app);
                });
        });
    });
});
