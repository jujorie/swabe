/* eslint-disable max-len */
'use strict';

describe('SwaggerAction', () => {
    const httpMocks = require('node-mocks-http');
    const {EventEmitter} = require('events');
    const SwaggerAction = require('../../lib/swagger-action.js');
    const SwaggerHttpError = require('../../lib/swagger-http-error');
    const removeFunctions = (actions) => {
        return JSON.parse(JSON.stringify(actions));
    };

    /**
     * @type {SwaggerAction}
     */
    let swaggerAction;

    beforeEach(() => {
        swaggerAction = SwaggerAction.create();
    });

    describe('Define actions', () => {
        const response = require('./swagger-action.result.json');

        it('Must define an action', () => {
            const actions = swaggerAction
                .define(
                    'getData',
                    {
                        description: 'fake description',
                        tags: ['tag1', 'tag2'],
                        summary: 'fake summary'
                    },
                    () => {}
                )
                .getSwaggerAction();

            expect(removeFunctions(actions)).toEqual(response);
        });

        it('Must define an action but using the flow interface', () => {
            const actions = swaggerAction
                .define('getData')
                .description('fake description')
                .summary('fake summary')
                .tags('tag1', 'tag2')
                .requestHandler(() => {})
                .getSwaggerAction();

            expect(removeFunctions(actions)).toEqual(response);
        });

        /* eslint-disable-next-line max-len */
        it('Must fail if specify description, summary, tags or requestHandler before define and action', () => {
            ['description', 'summary', 'tags', 'requestHandler']
                .forEach((methodName) => {
                    try {
                        swaggerAction[methodName]('FAKE');
                        throw new Error('Fail');
                    } catch (err) {
                        expect(err.message)
                            .toBe('You must define a action first');
                    }
                });
        });

        it('Must define a action with same name but with params', () => {
            const correct = require('./swagger-action-4.result.json');
            const result = swaggerAction
                .define('getData')
                .description('fake description')
                .requestHandler(() => {})

                .define('getData')
                .pathParameter('id', 'data id')
                .description('fake description')
                .requestHandler(() => {})

                .getSwaggerAction();

            expect(removeFunctions(result))
                .toEqual(removeFunctions(correct));
        });
    });

    describe('When ation has parameter', () => {
        it('Must define parameters', () => {
            const correct = require('./swagger-action-1.result.json');
            const result = swaggerAction
                .define('test')
                .parameter('name')
                .parameter('name1')
                .bodyJsonParameter('body')
                .pathParameter('path')
                .queryParameter('query')
                .getSwaggerAction();

            expect(removeFunctions(result))
                .toEqual(removeFunctions(correct));
        });

        it('Must define referenced parameters', () => {
            const correct = require('./swagger-action-2.result.json');
            const result = swaggerAction
                .define('test')
                .refParameter('ref1')
                .refParameter('ref2')
                .getSwaggerAction();

            expect(removeFunctions(result))
                .toEqual(removeFunctions(correct));
        });
    });

    describe('When define several actions', () => {
        it('Must define 2 actions', () => {
            const correct = require('./swagger-action-3.result.json');
            const result = swaggerAction
                .define('test1')
                .tags('tag1')
                .tags('tag1.1')
                .parameter('name')

                .define('test2')
                .tags('tag2')

                .getSwaggerAction();

            expect(removeFunctions(result))
                .toEqual(removeFunctions(correct));
        });
    });

    describe('When using swagger params', () => {
        it('Must get params from swageer params', () => {
            const params = SwaggerAction.getSwaggerParams({
                swagger: {
                    params: {
                        param1: {
                            value: '1'
                        },
                        param2: {
                            value: '2'
                        },
                        param3: {}
                    }
                }
            });

            expect(params).toEqual({
                param1: '1',
                param2: '2'
            });
        });
    });

    describe('When get json response', () => {
        describe('Using test cases', () => {
            const testCases = [
                {
                    fn: async () => 1,
                    result: '1',
                    status: 200
                },
                {
                    fn: async () => null,
                    result: '',
                    status: 200
                },
                {
                    fn: async () => {
                        throw new Error('error');
                    },
                    result: '{"error":"error"}',
                    status: 400
                },
                {
                    fn: async () => {
                        throw new SwaggerHttpError('error', 403);
                    },
                    result: '{"error":"error"}',
                    status: 403
                },
                {
                    fn: async () => {
                        throw new SwaggerHttpError('error');
                    },
                    result: '{"error":"error"}',
                    status: 400
                }
            ];

            testCases
                .forEach((testCase) => {
                    it(`Test for result ${testCase.result}`, (done) => {
                        const response = SwaggerAction
                            .jsonResponse(testCase.fn);
                        const req = httpMocks.createRequest();
                        const res = httpMocks.createResponse({
                            eventEmitter: EventEmitter
                        });
                        res.on('end', () => {
                            expect(res.getHeader('content-type'))
                                .toBe('application/json');
                            expect(res._getData()).toBe(testCase.result);
                            expect(res.statusCode).toBe(testCase.status);
                            done();
                        });
                        response(req, res);
                    });
                });
        });
    });

    describe('When call the action', () => {
        it('Must call the default function', (done) => {
            const action = SwaggerAction
                .create(SwaggerAction.defaultResponses)
                .define('test')
                .getSwaggerAction();

            const req = httpMocks.createRequest();
            const res = httpMocks.createResponse({
                eventEmitter: EventEmitter
            });
            res.on('end', () => {
                done();
            });

            action.test(req, res);
        });
    });

    describe('When define a response codes', () => {
        const testCases = require('./swagger-action-1.testcases');
        testCases
            .filter((testCase) => true !== testCase.exclude)
            .forEach((testCase) => {
                it(`Must get action of ${JSON.stringify(testCase.defaultResponse)}`, () => {
                    const action = SwaggerAction
                        .create(testCase.defaultResponse)
                        .define('getTest')
                        .response(200, 'sample', testCase.responseSchema)
                        .getSwaggerAction();
                    ;
                    const swAction = removeFunctions(action);
                    expect(swAction)
                        .toEqual(testCase.expected);
                });
            });
    });

    describe('When specify an Fixed URL', () => {
        it(`Must generate an action`, () => {
            const expected = {
                '_metadata': {
                    'getTest': {
                        'x-url': 'test/test'
                    }
                }
            };

            const action = SwaggerAction
                .create({})
                .define('getTest')
                .url('test/test')
                .getSwaggerAction();

            const swAction = removeFunctions(action);
            expect(swAction).toEqual(expected);
        });
    });
});
