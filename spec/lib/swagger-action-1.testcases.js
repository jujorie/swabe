
const schema = {
    'type': 'object',
    'properties': {
        'id': {
            'type': 'number'
        }
    }
};

module.exports = [
    {
        defaultResponse: {},
        responseSchema: schema,
        expected: {
            '_metadata': {
                'getTest': {
                    'responses': {
                        '200': {
                            'description': 'sample',
                            'schema': schema
                        }
                    }
                }
            }
        }
    },
    {
        defaultResponse: {
            responses: {
                '200': {
                    description: 'DEFAULT',
                    schema
                }
            }
        },
        responseSchema: {
            type: 'number'
        },
        expected: {
            '_metadata': {
                'getTest': {
                    'responses': {
                        '200': {
                            'description': 'sample',
                            'schema': {
                                type: 'number'
                            }
                        }
                    }
                }
            }
        }
    }
];
