'use strict';

((module) => {
    /* eslint-disable-next-line max-len */
    const SwaggerAction = require('../../../lib/swagger-action');

    module.exports = SwaggerAction.create()

        .define('getStatus', {})
        .description('Get api status')
        .summary('Get the current status of the API')
        .requestHandler(SwaggerAction.jsonResponse(() => {
            return 'OK';
        }))

        .getSwaggerAction();
})(module);
