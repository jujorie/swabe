'use strict';

describe('swaggerBuilder', () => {
    let builder;
    let builderSpy;
    const SwaggerBuilder = require('../../lib/swagger-builder.js');
    const {resolve} = require('path');

    beforeEach(() => {
        builderSpy = jasmine.createSpy('builderSpy');
        builder = SwaggerBuilder.create(
            {
                paths: {}
            },
            {
                builders: [
                    builderSpy
                ],
                swaggerRouterOptions: {}
            }
        );
    });

    describe('When is creatings urls', () => {
        it('Must create a short name if the action name equals root', () => {
            const url = builder._buildActionUrl('action', 'action', {});
            expect(url).toBe('/action');
        });

        it('Must create an action url without params', () => {
            const url = builder._buildActionUrl('api/controller', 'action', {});
            expect(url).toBe('/api/controller/action');
        });

        it('Must create an action url with path params', () => {
            const url = builder._buildActionUrl('api/controller', 'action', {
                parameters: [
                    {name: 'p1', in: 'path'},
                    {name: 'p2', in: 'path'},
                    {name: 'inhead', in: 'header'}
                ]
            });
            expect(url).toBe('/api/controller/action/{p1}/{p2}');
        });

        it('Must create an action url from x-url', () => {
            const url = builder._buildActionUrl('api/controller', 'action', {
                'x-url': '/other/thing'
            });
            expect(url).toBe('/api/other/thing');
        });

        it('Must create an action url from x-url', () => {
            const url = builder._buildActionUrl('.', 'action', {
                'x-url': '/root/thing'
            });
            expect(url).toBe('/root/thing');
        });
    });

    describe('Generate url and actions', () => {
        it('Must create a valid url from the controller filename', () => {
            const url = builder._buildUrlFromControllerFilename(
                'dir/dir/dir/Part1Part2Part3.controller.js'
            );
            expect(url).toBe('part1/part2/part3');
        });

        it('Must create a valid url from the controller filename with sub path',
            () => {
                builder._controllersRoot = 'dir/dir/';
                const url = builder._buildUrlFromControllerFilename(
                    'dir/dir/dir/Part1Part2Part3.controller.js'
                );
                expect(url).toBe('dir/part1/part2/part3');
            }
        );

        it('Must to get verb and action from method name', () => {
            [
                ['getAction', 'get', 'action'],
                ['putAction', 'put', 'action'],
                ['patchAction', 'patch', 'action'],
                ['deleteAction', 'delete', 'action'],
                ['updateAction', 'update', 'action'],
                ['optionsAction', 'options', 'action']
            ].forEach((line) => {
                const result = builder._getVerbAndActionFromMethodName(line[0]);
                if (result) {
                    const {verb, action} = result;
                    expect(verb).toBe(line[1]);
                    expect(action).toBe(line[2]);
                }
            });

            const result = builder._getVerbAndActionFromMethodName('xxxAction');
            expect(result).toBeNull();
        });
    });

    describe('When has to create an action', () => {
        let actions;
        let swaggerObject;

        beforeEach(() => {
            actions = {
                getAction: () => {},
                _metadata: {
                    getAction: {
                        parameters: {
                            in: {
                                name: 'aaa',
                                in: 'query'
                            }
                        }
                    }
                }
            };

            swaggerObject = {
                'x-swagger-router-controller': 'controllerCtrl',
                'get': {
                    'operationId': 'getAction',
                    'parameters': {
                        'in': {
                            'name': 'aaa',
                            'in': 'query'
                        }
                    }
                }
            };
        });

        it('Must return null with invalid acion name', () => {
            builder._createAction(
                'api/root',
                'controllerCtrl',
                'xxxAction',
                actions
            );

            expect(builder._swaggerObject.paths).toEqual({});
        });

        it('Must update action if the path exists', () => {
            builder._swaggerObject.paths['/api/root/action'] = {
                'get': {
                    'operationId': 'getAction',
                    'parameters': {
                        'in': {
                            'name': 'aaa',
                            'in': 'query'
                        }
                    }
                }
            };

            builder._createAction(
                'api/root',
                'controllerCtrl',
                'getAction',
                actions
            );
            const action = builder._swaggerObject.paths['/api/root/action'];
            expect(builderSpy).toHaveBeenCalled();
            expect(action).toBeDefined();
        });

        it('must create action if in doc there is no path', () => {
            delete builder._swaggerObject.paths;
            builder._createAction(
                'api/root',
                'controllerCtrl',
                'getAction',
                actions
            );
            const action = builder._swaggerObject.paths['/api/root/action'];

            expect(builderSpy).toHaveBeenCalled();
            expect(action).toBeDefined();
            expect(action).toEqual(swaggerObject);
        });

        it('Must create a action', () => {
            builder._createAction(
                'api/root',
                'controllerCtrl',
                'getAction',
                actions
            );
            const action = builder._swaggerObject.paths['/api/root/action'];

            expect(builderSpy).toHaveBeenCalled();
            expect(action).toBeDefined();
            expect(action).toEqual(swaggerObject);
        });
    });

    describe('Build swagger doc', () => {
        const correctResponses = require('./swagger-builder-1.result');

        describe('When using default params', () => {
            it('Must create default builder', () => {
                const bldr = new SwaggerBuilder({});
                expect(bldr._builders).toEqual([]);
                expect(bldr._swaggerRouterOptions).toBeNull();
            });

            it('Must create default builder using helper', () => {
                const bldr = SwaggerBuilder.create({});
                expect(bldr._builders).toEqual([]);
                expect(bldr._swaggerRouterOptions).toBeNull();
            });
        });

        describe('Using test cases', () => {
            const testCases = [
                {
                    paths: resolve(__dirname, './controllers/**/*.js'),
                    options: {},
                    doc: {}
                },
                {
                    paths: resolve(__dirname, './controllers/**/*.js'),
                    doc: {}
                },
                {
                    paths: resolve(__dirname, './controllers/**/*.js'),
                    options: {
                        controllers: resolve(__dirname, './controllers/**/*.js')
                    },
                    doc: {}
                },
                {
                    paths: [resolve(__dirname, './controllers/**/*.js')],
                    options: {
                        controllers: [
                            resolve(__dirname, './controllers/**/*.js')
                        ]
                    },
                    doc: {}
                }
            ];

            testCases.forEach((testCase, testCaseindex) => {
                const {doc, options, paths} = testCase;
                const correct = correctResponses[`${testCaseindex}`];
                it(`Test case ${testCaseindex}`, () => {
                    SwaggerBuilder
                        .create(
                            doc,
                            {
                                swaggerRouterOptions: options
                            }
                        )
                        .build({
                            path: paths
                        });

                    expect(doc).toEqual(correct.doc);
                    if (correct.options && correct.options.controllers) {
                        expect(options.controllers.length)
                            .toEqual(correct.options.controllers.length);
                    }
                });
            });
        });
    });
});
