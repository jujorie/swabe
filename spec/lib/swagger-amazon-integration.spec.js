'use strict';

describe('SwaggerAmazonIntegration', () => {
    /* eslint-disable-next-line max-len */
    const swaggerAmazonIntegration = require('../../lib/swagger-amazon-integration');

    it('Must add amazon swagger settings with no params', () => {
        const correct = require('./swagger-amazon-integration-1');
        const metadata = {};
        swaggerAmazonIntegration('post', '/controller/action/{id}', metadata);
        expect(metadata).toEqual(correct);
    });

    it('Must add amazon swagger settings with params', () => {
        const correct = require('./swagger-amazon-integration-2');
        const metadata = {
            parameters: [
                {in: 'path', name: 'inPath'},
                {in: 'query', name: 'inQuery'},
                {in: 'header', name: 'inHeader'}
            ]
        };
        swaggerAmazonIntegration('post', '/controller/action/{id}', metadata);
        expect(metadata).toEqual(correct);
    });

    it('Must add amazon swagger settings with referenced params', () => {
        const correct = require('./swagger-amazon-integration-3');
        const metadata = {
            parameters: [
                {$ref: 'path/data'}
            ]
        };
        swaggerAmazonIntegration('post', '/controller/action/{id}', metadata, {
            path: {
                data: {in: 'path', name: 'inPath'}
            }
        });
        expect(metadata).toEqual(correct);
    });

    it('Using mock response', () => {
        const correct = require('./swagger-amazon-integration-4');
        const metadata = {
            parameters: [
                {$ref: 'path/data'}
            ]
        };
        swaggerAmazonIntegration(
            'options',
            '/controller/action/{id}',
            metadata,
            {
                path: {
                    data: {in: 'path', name: 'inPath'}
                }
            }
        );
        expect(metadata).toEqual(correct);
    });

    it('Must fail with invalid IN param', () => {
        const metadata = {
            parameters: [
                {in: 'xxx', name: 'inPath'}
            ]
        };
        try {
            swaggerAmazonIntegration(
                'post',
                '/controller/action/{id}',
                metadata
            );
            throw new Error('__unexpected__');
        } catch (err) {
            expect(err.message)
                .toBe(`Invalid in param 'xxx'`);
        }
    });
});
