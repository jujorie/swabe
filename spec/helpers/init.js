'use strict';

((context) => {
    const JasmineConsoleReporter = require('jasmine-console-reporter');
    const reporter = new JasmineConsoleReporter({});

    context.clearReporters();
    context.addReporter(reporter);
})(jasmine.getEnv());
